# Learning-notes-and-materials

- basic-compute-public_knowledge
  - [计算机组成原理](/Basic_computer_knowledge/计算机组成原理/计算机组成原理.md)
- front-end
  - [浏览器实现原理与API](/front_end/浏览器的实现原理与API/浏览器.txt)
  - [CSS](/front_end/CSS/css.txt)
  - [HTML](/front_end/HTML/html.txt)
  - [JavaScript](/front_end/JavaScript/javeScript.txt)
- public-knowledge
  - [Chrome-DevTools](/public_knowledge/Chrome_DevTools/Chrome-DevTools.md)
  - [Vocabulary-Manual](/public_knowledge/English/VocabularyManual.md)
  - [Git](/public_knowledge/Git/Git.md)
  - [markdown](/public_knowledge/markdown/markdown_knowledge.md)
  - [RegRxp](/public_knowledge/regular_expression/正则表达式.md)
- Source editor
  - [VSCode](/source_editor/VSCode/VisualStudioCode.md)
