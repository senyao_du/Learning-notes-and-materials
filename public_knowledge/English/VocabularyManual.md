---
title: Vocabulary Manual
author: DSY
keyword: Vocabulary
---

<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->

<!-- code_chunk_output -->

- [Vocabulary Manual](#vocabulary-manual)
  - [一. Common Words](#一-common-words)
  - [二. Computer Words](#二-computer-words)

<!-- /code_chunk_output -->

# Vocabulary Manual

---

## 一. Common Words

- **knowledge**
  - n. 知识; 学问; 学识; 知晓; 知悉; 了解

- **public**
  - adj. 平民的; 大众的; 公众的; 百姓的; 公共的; 公立的; 政府的; 有关政府所提供服务的
  - n. 平民; 百姓; 民众; 志趣相同(或从事同一类活动)的群体

## 二. Computer Words

- **snippet**
  - n. 代码段
